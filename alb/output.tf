output "load_balancer_name" {
  value = aws_lb.alb.name
}

output "target_group_arn" {
  value = aws_lb_target_group.target_group.arn
}