data "aws_vpc" "vpc" {
  tags = {
    segment = var.segment
    region  = var.region
  }
}

data "aws_subnet_ids" "subnets" {
  vpc_id = data.aws_vpc.vpc.id
}

resource "aws_security_group" "ssh_http" {
  name   = "ssh-http-sg"
  vpc_id = data.aws_vpc.vpc.id
  tags = {
    Name = "ssh-http-sg"
  }
}

resource "aws_security_group_rule" "ssh_http_ingress_22" {
  description       = "TLS from VPC"
  from_port         = 80
  to_port           = 80
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ssh_http.id
  type              = "ingress"

}

resource "aws_security_group_rule" "ssh_http_ingress_80" {
  description       = "TLS from VPC"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ssh_http.id
  type              = "ingress"
}

resource "aws_security_group_rule" "ssh_http_egress_443" {
  description       = "TLS from VPC"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.ssh_http.id
  type              = "egress"
}