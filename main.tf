provider "aws" {
  region = "us-east-1"
}

module "key_pair" {
  source = "./kp"
}

module "vpc" {
  source  = "./vpc"
  segment = "test"
  region  = "us-east-1"
}

module "asg" {
  source            = "./launch_config"
  key_name          = module.key_pair.key_name
  security_groups   = [module.vpc.vpc_info["ssh_http"]]
  subnets           = module.vpc.vpc_info["subnets"]
  load_balancer     = module.alb.load_balancer_name
  target_group_arn = module.alb.target_group_arn
  depends_on        = [module.alb]
}

module "alb" {
  source          = "./alb"
  security_groups = [module.vpc.vpc_info["ssh_http"]]
  subnets         = module.vpc.vpc_info["subnets"]
  vpc_id          = module.vpc.vpc_info["vpc_id"]
}

