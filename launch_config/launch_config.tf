data "aws_ami" "amazon_linux_2" {
  most_recent = true
  filter {
    name   = "name"
    values = ["*amzn2-ami-hvm-2*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["137112412989"] # Canonical
}

resource "aws_launch_template" "launch_template" {
  name_prefix            = "terraform-launch-template"
  image_id               = data.aws_ami.amazon_linux_2.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = var.security_groups
  key_name               = var.key_name
  user_data              = filebase64("./bootstrap.sh")
}
data "aws_subnet" "subnet" {
  for_each = toset(var.subnets)
  id       = each.key
}

resource "aws_autoscaling_group" "auto_scaling_group" {
  name               = "terraform-auto-scaling-group"
  availability_zones = [for s in data.aws_subnet.subnet : s.availability_zone]
  desired_capacity   = 5
  max_size           = 10
  min_size           = 3

  launch_template {
    id      = aws_launch_template.launch_template.id
    version = "$Latest"
  }
  target_group_arns = [var.target_group_arn]
}